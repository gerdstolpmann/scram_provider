### !!!
### Currently needs the git version of ocamlnet (fixes in header parsing)

PACKAGES = nethttpd netstring netplex nettls-gnutls
SOURCES = atypes.ml settings.ml util.ml database.ml \
          sessions.ml req_auth.ml req_login.ml
SOURCES_MAIN = server.ml

.PHONY: all clean

all: paranut

paranut: $(SOURCES) $(SOURCES_MAIN)
	ocamlfind ocamlopt -o paranut -package "$(PACKAGES)" -linkpkg $(SOURCES) $(SOURCES_MAIN)

paranut.cma: $(SOURCES)
	ocamlfind ocamlc -o paranut.cma -a -package "$(PACKAGES)" -linkpkg $(SOURCES)


clean:
	rm -f *.cmi *.cmx *.o
	rm -f paranut
