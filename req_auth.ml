open Atypes
open Printf

(* TODO:
   - digest: set user lookup function
   - digest: store H(A1), not the password
   - digest: test
   - digest: check for conformance
   - digest: clean up Netmech_digest, write mli

   - scram and digest: additionally encrypt stored pw
   - scram and digest: hide password in random string
 *)

type authreq_params =
  { req_method : string;
    req_uri : string
  }

let auth_nego_period = 300.0
(* authentication has to be complete within this period of time *)

let hmac key =
  Netsys_digests.hmac (Netsys_digests.iana_find `SHA_256) key

let hmac_string key str =
  let dg = hmac key in
  Netsys_digests.digest_string dg str

let enable_scram (cgi : Netcgi.cgi_activation) =
  Settings.enable_http_scram_sha256 &&
    cgi # argument_value ~default:"true" "enable_scram_sha_256" = "true"

let enable_digest (cgi : Netcgi.cgi_activation) =
  Settings.enable_http_digest_md5 &&
    cgi # argument_value ~default:"true" "enable_digest_md5" = "true"

let new_session_id() =
  let sid_b = Bytes.create 16 in
  Netsys_rng.fill_random sid_b;
  let sid = Bytes.to_string sid_b in
  eprintf "New session: %s\n%!" (Util.b64_enc sid);
  sid

let progress ?(initial=false) ?(scram=[]) ?(digest=[]) () =
  InProgress
    { initial;
      scram;
      digest;
    }

let create_random() =
  let s = Bytes.make 16 ' ' in
  Netsys_rng.fill_random s;
  Digest.to_hex (Bytes.to_string s)

let unquote =
  Nethttp.Header.value_of_param

let auth_digest_get username =
  Some ["password","abc",[]]

let auth_digest_create_session() =
  let sprofile =
    { Netmech_digest.ptype = `HTTP;
      hash_functions = [ `MD5 ];
      mutual = true;
    } in
  let snonce_b = Bytes.create 16 in
  Netsys_rng.fill_random snonce_b;
  let snonce = Bytes.to_string snonce_b |> Util.b64_enc in
  { Netmech_digest.sstate = `Emit;
    srealm = Some Settings.realm;
    snonce;
    sresponse = None;
    snextnc = 1;
    sstale = false;
    sprofile;
    sutf8 = true;
    snosess = true;
    lookup = (fun user authz -> None);
  }

let auth_www_authenticate  (cgi : Netcgi.cgi_activation) params =
  cgi # set_header
      ~status:`Unauthorized
      ~cache:`No_cache
      ~content_type:"text/html; charset=\"utf-8\""
      ();
  (* TODO: this sets a single WWW-Authenticate header with multiple challenges.
     This is probably not supported by every client. Better create multiple
     headers. *)
  Nethttp.Header.set_www_authenticate
    cgi # environment # output_header params;
  Util.begin_page cgi "Unauthorized";
  Util.end_page cgi;
  cgi # output # commit_work()

let enc_digest_params params =
  List.map
    (fun (k,v) ->
      match k with
        | "algorithm" | "qop" | "nc" -> (k, `Q v)
        | _ -> (k, `V v)
    )
    params

let new_session_id() =
  let sid_b = Bytes.create 16 in
  Netsys_rng.fill_random sid_b;
  let sid = Bytes.to_string sid_b in
  eprintf "New session: %s\n%!" (Util.b64_enc sid);
  sid

let auth_any_www_authenticate ?(stale_digest=false)
                              (cgi : Netcgi.cgi_activation) =
  let sid = new_session_id() in
  let scram, scram_params =
    if enable_scram cgi && not stale_digest then
      let profile =
        { Netmech_scram.ptype = `SASL;
          hash_function = `SHA_256;
          return_unknown_user = false;
          iteration_count_limit = 100000;
        } in
      let scram =
        Netmech_scram.create_server_session profile (fun _ -> raise Not_found) in
      ([scram], ["SCRAM-SHA-256", [ "realm", `V Settings.realm ]])
    else
      ([], []) in
  let digest, digest_params =
    if enable_digest cgi then
      let digest = auth_digest_create_session() in
      let digest, kv = Netmech_digest.server_emit_initial_challenge_kv digest in
      let kv = if stale_digest then ("stale", "true") :: kv else kv in
      ([digest],
       ["Digest",
        enc_digest_params
          (("opaque", Util.b64_enc sid) :: kv)
       ]
      )
    else
      ([], []) in
  let state =
    InProgress { initial=true; scram; digest } in
  let valid =
    Unix.time() +. auth_nego_period in
  let session = { sid; state; valid } in
  Hashtbl.replace Sessions.sessions session.sid session;
  auth_www_authenticate cgi (scram_params @ digest_params)

let auth_logged_in (cgi : Netcgi.cgi_activation)
                   user_id start_sid auth_info_opt =
  let set_cookies =
    match start_sid with
      | Some sid ->
          [ Netcgi.Cookie.make
              ~max_age:Settings.ticket_lifetime
              ~path:"/"
              ~secure:true
              Sessions.cookie_name
              sid
          ]
      | None ->
          [] in
  let fields =
    match auth_info_opt with
      | None -> []
      | Some auth_info ->
          let h = new Netmime.basic_mime_header [] in
          Nethttp.Header.set_authentication_info h auth_info;
          [ "Authentication-Info", h # multiple_field "Authentication-Info"] in
  cgi # set_header
      ~status:`Ok
      ~cache:`No_cache
      ~content_type:"text/html; charset=\"utf-8\""
      ~set_cookies
      ~fields
      ();
  Util.begin_page cgi "Logged in";
  cgi # output # output_string (Util.encode ("User name: " ^ user_id));
  Util.end_page cgi;
  cgi # output # commit_work()

let auth_scram_emit (cgi : Netcgi.cgi_activation) session scram_list =
  assert(scram_list <> []);
  let resp =
    List.map
      (fun scram ->
        if Netmech_scram.server_error_flag scram then
          []
        else
          let scram, msg = Netmech_scram.server_emit_message scram in
          [ scram, msg ]
      )
      scram_list
    |> List.flatten in
  if resp = [] then raise Not_found;
  match
    List.find (fun (scram,_) -> Netmech_scram.server_finish_flag scram) resp
  with
    | (scram, msg) ->
        let user_id =
          match Netmech_scram.server_user_name scram with
            | Some u -> u
            | None -> assert false in
        let db = Database.get() in
        let user = SMap.find user_id db.db_users in
        let valid = Unix.time() +. float Settings.ticket_lifetime in
        let session = { session with state = Authenticated user; valid } in
        Hashtbl.replace Sessions.sessions session.sid session;
        auth_logged_in
          cgi user_id (Some session.sid)
          (Some
             [ "sid", `Q (Util.b64_enc session.sid);
               "data", `Q (Util.b64_enc msg)
             ]
          )
    | exception Not_found ->
        (* all scram sessions in scram_list lead to the same response message
           because the same nonce is used!
         *)
        let msg = snd (List.hd resp) in
        let state =
          progress ~scram:(List.map fst resp) () in
        let session = { session with state } in
        Hashtbl.replace Sessions.sessions session.sid session;
         auth_www_authenticate
          cgi
          ["SCRAM-SHA-256",
           [ "sid", `Q (Util.b64_enc session.sid);
             "data", `Q (Util.b64_enc msg)
           ]
          ]

let auth_scram (cgi : Netcgi.cgi_activation) mech_params =
  let d_b64 = List.assoc "data" mech_params |> unquote in (* or Not_found *)
  let d = Util.b64_dec_nf d_b64 in
  if !Util.debug then
    eprintf "Decoded data parameter: %s\n%!" d;
  match List.assoc "sid" mech_params with
    | sid_b64 ->
        let sid =
          try Util.b64_dec_nf (unquote sid_b64)
          with Invalid_argument _ -> raise Not_found in
        let session = Hashtbl.find Sessions.sessions sid in
        if Unix.time() > session.valid then raise Not_found;
        ( match session.state with
            | InProgress p ->
                if p.scram = [] || p.initial then raise Not_found;
                if !Util.debug then
                  eprintf "Found session sid=%s\n%!" (unquote sid_b64);
                let scram =
                  List.map
                    (fun scram ->
                      Netmech_scram.server_recv_message scram d
                    )
                    p.scram in
                let state = progress ~scram () in
                let session = { session with state } in
                Hashtbl.replace Sessions.sessions sid session;
                auth_scram_emit cgi session scram
            | Authenticated _ ->
                (* bad client message! *)
                raise Not_found
        )
    | exception Not_found ->
        ( match List.assoc "realm" mech_params |> unquote with
            | r ->
                if r <> Settings.realm then raise Not_found
            | exception Not_found ->
                ();
        );
        let sid = new_session_id() in
        let profile =
          { Netmech_scram.ptype = `SASL;
            hash_function = `SHA_256;
            return_unknown_user = false;
            iteration_count_limit = 100000;
          } in
        let nonce = create_random() in
        let scram0 =
          Netmech_scram.create_server_session
            ~nonce profile (fun _ -> raise Not_found)
          |> (fun scram -> Netmech_scram.server_recv_message scram d) in
        let user =
          match Netmech_scram.server_user_name scram0 with
            | None -> assert false
            | Some user -> user in
        let db = Database.get() in
        let keys = Database.keys db user T_SCRAM_SHA256 in
        let scram_list =
          if keys = [] then
            [scram0]  (* will never be successful *)
          else
            List.map
              (fun key ->
                match key.key_creds with
                  | SCRAM_SHA256 p ->
                      Netmech_scram.create_server_session
                        ~nonce profile
                        (fun _ ->
                          `Stored_creds(p.stored_key, p.server_key, p.salt,
                                        p.iterations)
                        )
                      |> (fun scram ->
                            Netmech_scram.server_recv_message scram d)
                  | _ -> assert false
              )
              keys in
        let state = progress ~scram:scram_list () in
        let valid =
          Unix.time() +. auth_nego_period in
        let session =
          { sid; state; valid } in
        Hashtbl.replace Sessions.sessions sid session;
        auth_scram_emit cgi session scram_list

let rec seq_find f seq =
  match seq() with
    | Seq.Nil -> raise Not_found
    | Seq.Cons(x, seq') -> if f x then x else seq_find f seq'

let seq_is_empty seq =
  match seq() with
    | Seq.Nil -> true
    | Seq.Cons _ -> false

let auth_digest_emit (cgi : Netcgi.cgi_activation) session digest_seq =
  assert(not (seq_is_empty digest_seq));
  let resp =
    Seq.flat_map
      (fun dg ->
        match Netmech_digest.(dg.sstate) with
          | `Auth_error msg ->
              if !Util.debug && msg <> "bad password" then
                eprintf "Digest auth error: %s\n%!" msg;
              Seq.empty
          | `Emit ->
              let dg, msg = Netmech_digest.server_emit_final_challenge_kv dg in
              Seq.return (dg, msg)
          | _ ->
              Seq.empty  (* strange... *)
      )
      digest_seq in
  if seq_is_empty resp then (
    if !Util.debug then
      eprintf "Digest: no password found\n%!";
    Hashtbl.remove Sessions.sessions session.sid;
    raise Not_found
  );
  match
    seq_find (fun (dg,_) -> Netmech_digest.(dg.sstate) = `OK) resp
  with
    | (dg, msg) ->
        let user_id =
          match Netmech_digest.(dg.sresponse) with
            | None -> assert false
            | Some (r,_,_) -> Netmech_digest.(r.r_user) in
        if !Util.debug then
          eprintf "Digest: authenticated as user %s\n%!" user_id;
        let db = Database.get() in
        let user = SMap.find user_id db.db_users in
        let valid = Unix.time() +. float Settings.ticket_lifetime in
        let session = { session with state = Authenticated user; valid } in
        Hashtbl.replace Sessions.sessions session.sid session;
        let auth_info_opt =
          if msg = [] then
            None  (* RFC-2069 mode *)
          else
            Some (enc_digest_params msg) in
        auth_logged_in
          cgi user_id (Some session.sid) auth_info_opt
    | exception Not_found ->
        (* all sessions in digest_list lead to the same response message
           because the same nonce is used!
         *)
        if !Util.debug then
          eprintf "Digest: continuing\n%!";
        let resp = List.of_seq resp in
        let _, msg = List.hd resp in
        let state =
          progress ~digest:(List.map fst resp) () in
        let session = { session with state } in
        Hashtbl.replace Sessions.sessions session.sid session;
         auth_www_authenticate
          cgi
          ["Digest", enc_digest_params msg]


let auth_digest (cgi : Netcgi.cgi_activation) authreq_params mech_params =
  (* "digest" is a server-first protocol, so there must already be a session *)
  let sid_b64 = List.assoc "opaque" mech_params in
  let sid =
    try Util.b64_dec_nf (unquote sid_b64)
    with Invalid_argument _ -> raise Not_found in
  let session = Hashtbl.find Sessions.sessions sid in
  if Unix.time() > session.valid then raise Not_found;
  ( match session.state with
      | InProgress p ->
          if p.digest = [] then raise Not_found;
          if !Util.debug then
            eprintf "Found session sid=%s\n%!" (unquote sid_b64);
          if !Util.debug then
            eprintf "Request method: %s\n%!" authreq_params.req_method;
          let mech_params = List.map (fun (n,v) -> (n,unquote v)) mech_params in
          (* We do not check the uri! *)
          let digest_seq =
            if p.initial then (
              assert(List.length p.digest = 1);
              let dg = List.hd p.digest in
              let username = List.assoc "username" mech_params in
              let db = Database.get() in
              let keys = Database.keys db username T_DIGEST_MD5 in
              if keys = [] then raise Not_found;
              List.to_seq keys
              |> Seq.flat_map
                   (fun key ->
                     match key.key_creds with
                       | DIGEST_MD5 p ->
                           let half1 = String.sub p.stored_key 0 8 in
                           let half2 = String.sub p.stored_key 8 8 in
                           Util.all_permutations half2
                           |> Seq.map
                                (fun half2 ->
                                  let h_pw = half1 ^ half2 in
                                  let creds =
                                    Some [ "digest",
                                           Util.b64_enc h_pw,
                                           ["realm", Settings.realm;
                                            "algo", "md5"
                                           ]
                                         ] in
                                  let dg =
                                    { dg with
                                      lookup = (fun user authz -> creds)
                                    } in
                                  Netmech_digest.server_process_response_kv
                                    dg mech_params authreq_params.req_method
                                )
                       | _ ->
                           assert false
                   )
            ) else (
              List.map
                (fun dg ->
                  Netmech_digest.server_process_response_kv
                    dg mech_params authreq_params.req_method
                )
                p.digest
              |> List.to_seq
            ) in
          (* we do not want to store the whole digest_seq in the session
             (can be long), so just clear out other mechanisms.
           *)
          let state = progress ~digest:p.digest () in
          let session = { session with state } in
          Hashtbl.replace Sessions.sessions sid session;
          auth_digest_emit cgi session digest_seq
      | Authenticated _ ->
          (* probably an attempt to re-authenticate, but we don't support it *)
          auth_any_www_authenticate ~stale_digest:true cgi
  )

let auth_any (cgi : Netcgi.cgi_activation) authreq_params =
  try
    let mech_name, mech_params =
      (* raises Not_found if unavailable *)
      Nethttp.Header.get_authorization cgi#environment#input_header in
    if !Util.debug then (
      eprintf "Authorization mechanism: %s\n%!" mech_name;
      List.iter
        (fun (n,v) -> eprintf "Authorization param: %s=%s\n%!" n (unquote v))
        mech_params
    );
    match String.lowercase_ascii mech_name with
      | "scram-sha-256" ->
          auth_scram cgi mech_params
      | "digest" ->
          auth_digest cgi authreq_params mech_params
      | _ ->
          raise Not_found
  with Not_found ->
    auth_any_www_authenticate cgi

let process (cgi : Netcgi.cgi_activation) authreq_params =
  if !Util.debug then (
    eprintf "REQUEST HEADER:\n%!";
    List.iter
      (fun (n,v) -> eprintf "  %s=%s\n%!" n v)
      cgi#environment#input_header#fields;
  );
  match None (* Sessions.check_auth cgi *) with
    | None ->
        auth_any cgi authreq_params;
        if !Util.debug then (
          eprintf "RESPONSE HEADER:\n";
          List.iter
            (fun (n,v) -> eprintf "  %s=%s\n%!" n v)
            cgi#environment#output_header#fields
        )
    | Some user ->
        auth_logged_in cgi user.user_id None None

let direct_authreq_params (cgi : Netcgi.cgi_activation) =
  { req_method = cgi # environment # cgi_request_method;
    req_uri = cgi # environment # cgi_property ~default:"/" "REQUEST_URI"
      (* Nethttpd sets REQUEST_URI *)
  }

let indirect_authreq_params (cgi : Netcgi.cgi_activation) =
  let req_method =
    try (cgi # argument "REQUEST_METHOD") # value
    with Not_found -> cgi # environment # cgi_request_method in
  let req_uri =
    try (cgi # argument "REQUEST_URI") # value
    with Not_found ->
      cgi # environment # cgi_property ~default:"/" "REQUEST_URI" in
  { req_method; req_uri }
