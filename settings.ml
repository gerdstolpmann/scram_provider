open Atypes

let to_int min s =
  let n = int_of_string s in
  if n < min then failwith "bad number";
  n

let path =
  try Sys.getenv "PARANUT_PATH"
  with Not_found -> "/usr/local/lib/paranut"

let instance =
  try Sys.getenv "PARANUT_INSTANCE" |> to_int 0
  with
    | Not_found -> 0
    | Invalid_argument _ | Failure _ ->
        failwith "bad environment variable: PARANUT_INSTANCE"

let dbkey =
  try Sys.getenv "PARANUT_DBKEY"
  with
    | Not_found ->
        failwith "missing environment variable: PARANUT_DBKEY"

let iterations =
  try Sys.getenv "PARANUT_ITERATIONS" |> to_int 4096
  with
    | Not_found -> 10000
    | Invalid_argument _ | Failure _ ->
        failwith "bad environment variable: PARANUT_ITERATIONS"

let realm =
  try Sys.getenv "PARANUT_REALM"
  with Not_found -> "Paranut login"

let ticket_lifetime =
  try Sys.getenv "PARANUT_TICKET_LIFETIME" |> to_int 300
  with Not_found -> 86400

let enable_http_scram_sha256 =
  try Sys.getenv "PARANUT_ENABLE_HTTP_SCRAM_SHA_256" = "true"
  with Not_found -> true

let enable_http_digest_md5 =
  try Sys.getenv "PARANUT_ENABLE_HTTP_DIGEST_MD5" = "true"
  with Not_found -> false

let () =
  (* remove sensitive information from the environment so that the
     worker subprocesses cannot read it anymore *)
  Unix.putenv "PARANUT_DBKEY" ""
