open Atypes
open Printf

let process (cgi : Netcgi.cgi_activation) =
  (* The [try] block catches errors during the page generation. *)
  try
    (* Set the header. The header specifies that the page must not be
     * cached. This is important for dynamic pages called by the GET
     * method, otherwise the browser might display an old version of
     * the page.
     * Furthermore, we set the content type and the character set.
     * Note that the header is not sent immediately to the browser because
     * we have enabled HTML buffering.
     *)
    cgi # set_header 
      ~cache:`No_cache 
      ~content_type:"text/html; charset=\"utf-8\""
      ();

    ( match cgi # environment # cgi_script_name with
        | "/auth/auth" ->
            Req_auth.process cgi (Req_auth.indirect_authreq_params cgi)
        | "/auth/login" ->
            Req_login.process cgi
        | _ -> raise Bad_request
    );

    (* After the page has been fully generated, we can send it to the
     * browser. 
     *)
    cgi # output # commit_work();
  with
    | Bad_request ->
        cgi # set_header
            ~status:`Bad_request
            ~cache:`No_cache
            ~content_type:"text/html; charset=\"utf-8\""
            ();
        Util.begin_page cgi "Bad request";
        Util.end_page cgi;
        cgi # output # commit_work()
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Exception: %s\n" (Printexc.to_string error);
        eprintf "Backtrace: %s\n%!" bt;
        (* An error has happened. Generate now an error page instead of
         * the current page. By rolling back the output buffer, any 
         * uncomitted material is deleted.
         *)
        cgi # output # rollback_work();

        (* We change the header here only to demonstrate that this is
         * possible.
         *)
        cgi # set_header 
          ~status:`Internal_server_error
          ~cache:`No_cache 
          ~content_type:"text/html; charset=\"utf-8\""
          ();

        Util.begin_page cgi "Software error";
        cgi # output # output_string "While processing the request an OCaml exception has been raised:<BR>";
        cgi # output # output_string ("<TT>" ^ Util.encode(Printexc.to_string error) ^ "</TT><BR>");
        Util.end_page cgi;

        (* Now commit the error page: *)
        cgi # output # commit_work()

let parallelizer =
  Netplex_mp.mp()  (* multi-processing *)

let get_unix_user config =
  try
    let name =
      match config#resolve_section config#root_addr "service" with
        | [ service ] ->
            let a = config#resolve_parameter service "user" in
            config#string_param a
        | _ -> raise Not_found in
    let pwe = Unix.getpwnam name in
    Unix.(pwe.pw_uid)
  with
    | Not_found ->
        Unix.getuid()

let get_unix_group config =
  try
    let name =
      match config#resolve_section config#root_addr "service" with
        | [ service ] ->
            let a = config#resolve_parameter service "group" in
            config#string_param a
        | _ -> raise Not_found in
    let gre = Unix.getgrnam name in
    Unix.(gre.gr_gid)
  with
    | Not_found ->
        Unix.getgid()

let add_admin_user cmdline_cfg id =
  let password = Netsys_posix.tty_read_password "New password: " in
  let password2 = Netsys_posix.tty_read_password "Re-type password: " in
  if password <> password2 then
    failwith "passwords do not match";
  let db = Database.get() in
  let config_file = Netplex_main.config_filename cmdline_cfg in
  let config = Netplex_config.read_config_file config_file in
  if not (Sys.file_exists db.db_filename) then (
    let unix_user = get_unix_user config in
    let unix_group = get_unix_group config in
    let fd =
      Unix.openfile db.db_filename Unix.[O_WRONLY; O_CREAT; O_TRUNC] 0o600 in
    Unix.fchown fd unix_user unix_group;
    Unix.close fd;
  );
  Database.add_admin_user db id password;
  Database.write db

let start() =
  let (opt_list, cmdline_cfg) = Netplex_main.args() in

  let add_admin = ref None in

  let opt_list' =
    [ "-add-admin-user", Arg.String (fun s -> add_admin := Some s),
      "<name>  Add this user to the db as admin (no server start)";

      "-debug", Arg.String (fun s -> Netlog.Debug.enable_module s),
      "<module>  Enable debug messages for <module>";

      "-debug-all", Arg.Unit (fun () -> Netlog.Debug.enable_all()),
      "  Enable all debug messages";

      "-debug-list", Arg.Unit (fun () -> 
                                 List.iter print_endline (Netlog.Debug.names());
                                 exit 0),
      "  Show possible modules for -debug, then exit";
    ] @ opt_list in

  Arg.parse 
    opt_list'
    (fun s -> raise (Arg.Bad ("Don't know what to do with: " ^ s)))
    "usage: netplex [options]";

  ( match !add_admin with
      | Some id ->
          add_admin_user cmdline_cfg id;
          exit 0
      | None ->
          ()
  );

  let authenticate =
    { Nethttpd_services.dyn_handler = (fun _ -> process);
      dyn_activation = Nethttpd_services.std_activation `Std_activation_buffered;
      dyn_uri = None;                 (* not needed *)
      dyn_translator = (fun _ -> ""); (* not needed *)
      dyn_accept_all_conditionals = false;
    } in
  let nethttpd_factory = 
    Nethttpd_plex.nethttpd_factory
      ~handlers:[ "authenticate", authenticate ]
      () in
  Netplex_main.startup
    parallelizer
    Netplex_log.logger_factories   (* allow all built-in logging styles *)
    Netplex_workload.workload_manager_factories (* ... all ways of workload management *)
    [ nethttpd_factory ]           (* make this nethttpd available *)
    cmdline_cfg
;;

Printexc.record_backtrace true;
Netsys_signal.init();
Nettls_gnutls.init();
start();;
