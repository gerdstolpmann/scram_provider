open Atypes
open Printf

let kv_line kv_list =
  String.concat "," (List.map (fun (k,v) -> sprintf "%s:%s" k v) kv_list)

let kv_split s =
  String.split_on_char ',' s
  |> (List.map
        (fun p ->
          match String.index p ':' with
            | i ->
                (String.sub p 0 i, String.sub p (i+1) (String.length p - i - 1))
            | exception Not_found ->
                failwith "badly formatted line"
        )
     )

let prep_key_digest_name = "SHA2-256"

let cipher_name = "AES-256"
let cipher_mode = "GCM"

let prep_key key =
  let prep_key_digest =
    try Netsys_digests.find prep_key_digest_name
    with Not_found ->
      failwith ("hash function not supported: " ^ prep_key_digest_name) in
  Netsys_digests.digest_string prep_key_digest key

let get_cipher() =
  try Netsys_ciphers.find (cipher_name, cipher_mode)
  with Not_found ->
    failwith (sprintf "cipher not supported: %s in %s mode"
                      cipher_name cipher_mode)

let write db =
  let file = db.db_filename ^ ".new" in
  let cipher = get_cipher() in
  let cipher_ctx = cipher # create (prep_key db.db_key) `None in
  let ch1 = new Netchannels.output_channel (open_out file) in
  let ch2 = Netchannels_crypto.encrypt_out cipher_ctx ch1 in
  let random = Bytes.create 8 in
  Netsys_rng.fill_random random;
  ch2 # output_bytes random;
  SMap.iter
    (fun _ u ->
      let u_line =
        [ "type", "user";
          "id", Util.b64_enc u.user_id;
          "full_name", Util.b64_enc u.user_full_name;
          "changed", sprintf "%.0f" u.user_changed;
          "enabled", string_of_bool u.user_enabled;
          "setpw", string_of_bool u.user_setpw;
          "groups", String.concat "|" (List.map Util.b64_enc u.user_groups);
        ] in
      ch2 # output_string (kv_line u_line);
      ch2 # output_string "\n";
      List.iter
        (fun key ->
          match key.key_creds with
            | SCRAM_SHA1 cred ->
                let c_line =
                  [ "type", "cred";
                    "key", Util.b64_enc key.key_id;
                    "app", Util.b64_enc key.key_app;
                    "mech", "SCRAM-SHA-1";
                    "stored_key", Util.b64_enc cred.stored_key;
                    "server_key", Util.b64_enc cred.server_key;
                    "salt", Util.b64_enc cred.salt;
                    "iterations", string_of_int cred.iterations
                  ] in
                ch2 # output_string (kv_line c_line);
                ch2 # output_string "\n";
            | SCRAM_SHA256 cred ->
                let c_line =
                  [ "type", "cred";
                    "key", Util.b64_enc key.key_id;
                    "app", Util.b64_enc key.key_app;
                    "mech", "SCRAM-SHA-256";
                    "stored_key", Util.b64_enc cred.stored_key;
                    "server_key", Util.b64_enc cred.server_key;
                    "salt", Util.b64_enc cred.salt;
                    "iterations", string_of_int cred.iterations
                  ] in
                ch2 # output_string (kv_line c_line);
                ch2 # output_string "\n";
            | DIGEST_MD5 cred ->
                let c_line =
                  [ "type", "cred";
                    "key", Util.b64_enc key.key_id;
                    "app", Util.b64_enc key.key_app;
                    "mech", "DIGEST-MD5";
                    "stored_key", Util.b64_enc cred.stored_key;
                    "realm", Util.b64_enc cred.realm
                  ] in
                ch2 # output_string (kv_line c_line);
                ch2 # output_string "\n";
        )
        u.user_keys;
    )
    db.db_users;
  ch2 # output_string "type:eof\n";
  ch2 # close_out();  (* also closes ch1 *)
  Sys.rename file db.db_filename

let read db =
  let cipher = get_cipher() in
  let cipher_ctx = cipher # create (prep_key db.db_key) `None in
  let ch1 = new Netchannels.input_channel (open_in db.db_filename) in
  let ch2 = Netchannels_crypto.decrypt_in cipher_ctx ch1 in
  try
    ignore(ch2 # really_input_string 8);
    let users = ref SMap.empty in
    let current_user = ref None in
    let append_key key =
      match !current_user with
        | None ->
            failwith "no current user"
        | Some user ->
            let user = { user with user_keys = user.user_keys @ [key] } in
            current_user := Some user;
            users := SMap.add user.user_id user !users in
    let eof = ref false in
    while not !eof do
      let line = ch2 # input_line() in
      if !Util.debug then
        eprintf "line: %s\n%!" line;
      let fields = kv_split line in
      match fields with
        | ("type", "eof") :: _ ->
            eof := true
        | ("type", "user") :: ("id", user_id) :: ("full_name", user_full_name)
          :: ("changed", user_changed) :: ("enabled", user_enabled)
          :: ("setpw", user_setpw) :: ("groups", user_groups) :: _ ->
            let user_groups =
              String.split_on_char '|' user_groups
              |> List.map Util.b64_dec in
            let user =
              { user_id = Util.b64_dec user_id;
                user_full_name = Util.b64_dec user_full_name;
                user_changed = float_of_string user_changed;
                user_enabled = bool_of_string user_enabled;
                user_setpw = bool_of_string user_setpw;
                user_groups;
                user_keys = [];
              } in
            current_user := Some user;
            users := SMap.add user.user_id user !users
        | ("type", "cred") :: ("key", key_id) :: ("app", key_app) :: cfields ->
            let creds =
              match cfields with
                | ("mech", "SCRAM-SHA-1") :: ("stored_key", stkey)
                    :: ("server_key", srvkey) :: ("salt", salt)
                    :: ("iterations", i) :: _ ->
                    SCRAM_SHA1 { stored_key = Util.b64_dec stkey;
                                 server_key = Util.b64_dec srvkey;
                                 salt = Util.b64_dec salt;
                                 iterations = int_of_string i
                               }
                | ("mech", "SCRAM-SHA-256") :: ("stored_key", stkey)
                  :: ("server_key", srvkey) :: ("salt", salt)
                  :: ("iterations", i) :: _ ->
                    SCRAM_SHA256 { stored_key = Util.b64_dec stkey;
                                   server_key = Util.b64_dec srvkey;
                                   salt = Util.b64_dec salt;
                                   iterations = int_of_string i
                                 }
                | ("mech", "DIGEST-MD5") :: ("stored_key", stkey)
                  :: ("realm", realm) :: _ ->
                    DIGEST_MD5 { stored_key = Util.b64_dec stkey;
                                 realm = Util.b64_dec realm
                               }
                | ("mech", mech) :: _ ->
                    failwith ("unsupported mechanism: " ^ mech)
                | _ ->
                    failwith "bad line" in
            let key =
              { key_id = Util.b64_dec key_id;
                key_app = Util.b64_dec key_app;
                key_creds = creds
              } in
            append_key key
        | ("type", t) :: _ ->
            failwith ("unsupported line type: " ^ t)
        | _ ->
            failwith "bad line"
    done;
    db.db_users <- !users
  with
    | End_of_file ->
        ch2 # close_in();
        failwith "cannot parse database: premature EOF"
    | Failure msg ->
        ch2 # close_in();
        failwith ("cannot parse database: " ^ msg)
    | Invalid_argument msg ->
        ch2 # close_in();
        failwith ("cannot parse database: " ^ msg)

let create_scram_sha256 password =
  let salt = Netmech_scram.create_salt() in
  let iterations = Settings.iterations in
  let stored_key, server_key =
    Netmech_scram.stored_key `SHA_256 password salt iterations in
  SCRAM_SHA256 { stored_key; server_key; salt; iterations  }

let create_scram_sha1 password =
  let salt = Netmech_scram.create_salt() in
  let iterations = Settings.iterations in
  let stored_key, server_key =
    Netmech_scram.stored_key `SHA_1 password salt iterations in
  SCRAM_SHA1 { stored_key; server_key; salt; iterations  }

let create_digest_md5 username password =
  let a1 = sprintf "%s:%s:%s" username Settings.realm password in
  let md5 = Netsys_digests.digest_string (Netsys_digests.iana_find `MD5) in
  let h_a1 = md5 a1 in
  let half1 = String.sub h_a1 0 8 in
  let half2 = String.sub h_a1 8 8 in
  let stored_key = half1 ^ Util.randomly_permute half2 in
  DIGEST_MD5 { stored_key; realm = Settings.realm }

let add_admin_user db id password =
  let keys =
    [ { key_id = "1";
        key_app = "main";
        key_creds = create_scram_sha256 password;
      };
      (* for the time being, also add a digest-md5 version: *)
      { key_id = "1";
        key_app = "main";
        key_creds = create_digest_md5 id password;
      };
    ] in
  let user =
    { user_id = id;
      user_full_name = "Admin user";
      user_changed = Unix.time();
      user_enabled = true;
      user_setpw = true;
      user_groups = [ "admin" ];
      user_keys = keys
    } in
  db.db_users <- SMap.add id user db.db_users

let current = ref None

let get() =
  match !current with
    | Some db -> db
    | None ->
        let db =
          { db_instance = Settings.instance;
            db_filename =
              sprintf "%s/paranut_%d.data" Settings.path Settings.instance;
            db_key = Settings.dbkey;
            db_users = SMap.empty
          } in
        if Sys.file_exists db.db_filename then
          read db;
        current := Some db;
        db

let keys db id t_cred =
  let user = SMap.find id db.db_users in
  List.filter
    (fun key ->
      match key.key_creds with
        | SCRAM_SHA1 _ -> t_cred = T_SCRAM_SHA1
        | SCRAM_SHA256 _ -> t_cred = T_SCRAM_SHA256
        | DIGEST_MD5 _ -> t_cred = T_DIGEST_MD5
    )
    user.user_keys
