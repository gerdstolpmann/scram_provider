let debug = ref true

let b64_enc = Netencoding.Base64.encode
let b64_dec s =
  match Netencoding.Base64.decode s with
    | u -> u
    | exception Invalid_argument _ ->
        failwith "base64 decode"

let b64_dec_nf s =
  match Netencoding.Base64.decode s with
    | u -> u
    | exception Invalid_argument _ ->
        raise Not_found
                 
let encode = Netencoding.Html.encode ~in_enc:`Enc_utf8()

let random_int16() =
  let b = Bytes.create 2 in
  Netsys_rng.fill_random b;
  256 * (Bytes.get b 0 |> Char.code) + (Bytes.get b 1 |> Char.code)

let randomly_permute s =
  let n = String.length s in
  let b = Buffer.create (String.length s) in
  let rec loop round available =
    if available = [] then
      ()
    else
      let k =
        if round = 0 && n > 1 then
          random_int16() mod (n-1) + 1
        else
          random_int16() mod (n-round) in
      let p = List.nth available k in
      let available = List.filter (fun q -> q <> p) available in
      Buffer.add_char b s.[p];
      loop (round+1) available in
  loop 0 (List.init n (fun k -> k));
  Buffer.contents b

let all_permutations s =
  let n = String.length s in
  let rec loop acc available =
    if available = [] then
      let acc = Array.of_list acc in
      String.mapi (fun i _ -> s.[acc.(i)]) s
      |> Seq.return
    else
      List.to_seq available
      |> Seq.flat_map
           (fun p ->
             let available = List.filter (fun q -> q <> p) available in
             loop (p :: acc) available
           ) in
  loop [] (List.init n (fun k -> k))

let begin_page cgi title =
  (* Output the beginning of the page with the passed [title]. *)
  let out = cgi # output # output_string in
  out "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n";
  out "<HTML>\n";
  out "<HEAD>\n";
  out ("<TITLE>" ^ encode title ^ "</TITLE>\n");
  out ("<STYLE TYPE=\"text/css\">\n");
  out "body { background: white; color: black; }\n";
  out "</STYLE>\n";
  out "</HEAD>\n";
  out "<BODY>\n";
  out ("<H1>" ^ encode title ^ "</H1>\n")
;;


let end_page cgi =
  let out = cgi # output # output_string in
  out "</BODY>\n";
  out "</HTML>\n"
;;
