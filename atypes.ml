exception Bad_request

module SMap = Map.Make(String)

type mech_type =
  | T_SCRAM_SHA1 | T_SCRAM_SHA256 | T_DIGEST_MD5

type creds =
  | SCRAM_SHA1 of { stored_key : string;
                    server_key : string;
                    salt : string;
                    iterations : int;
                  }
  | SCRAM_SHA256 of { stored_key : string;
                      server_key : string;
                      salt : string;
                      iterations : int;
                    }
  | DIGEST_MD5 of { realm : string;
                    stored_key : string
                  }

type key =
  { key_id : string;   (* random *)
    key_app : string;  (* application name *)
    key_creds : creds;
  }

type user =
  { user_id : string;
    user_full_name : string;
    user_changed : float;
    user_enabled : bool;
    user_setpw : bool;
    user_groups : string list;
    user_keys : key list;
  }

type database =
  { db_instance : int;  (* 1, 2, ... *)
    db_filename : string;
    db_key : string;
    mutable db_users : user SMap.t;
  }

type state =
  | InProgress of { initial : bool;
                    scram : Netmech_scram.server_session list;
                    digest : Netmech_digest.server_session list;
                  }
  | Authenticated of user

type session =
  { sid : string;
    state : state;
    valid : float;
  }

