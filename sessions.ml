open Atypes
open Printf

let sessions =
  Hashtbl.create 43

let cookie_name =
  "AUTHID"

let check_auth (cgi : Netcgi.cgi_activation) =
  match cgi # environment # cookie cookie_name with
    | cookie ->
        let sid = Netcgi.Cookie.value cookie in
        ( match Hashtbl.find sessions sid with
            | session ->
                if Unix.time() > session.valid then
                  None
                else
                  ( match session.state with
                      | InProgress _ -> None
                      | Authenticated user -> Some user
                  )
            | exception Not_found ->
                None
        )
    | exception Not_found ->
        None

let gc () =
  (* TODO: call this now and then *)
  let now = Unix.time() in
  let to_remove =
    Hashtbl.fold
      (fun _ session acc ->
        if now > session.valid then session.sid :: acc else acc
      )
      sessions
      [] in
  List.iter
    (fun sid ->
      Hashtbl.remove sessions sid
    )
    to_remove
