function utf8In16Encode(str) {
    // convert a string to a binary string of the UTF-8 representation
    return encodeURIComponent(str).replace( /%([0-9A-F]{2})/g,
        function(match, p1) {
            return String.fromCharCode('0x' + p1);
        })
}

function binary(u) {
    // convert a binary string (each UTF-16 char is seen as a byte) to a true
    // Uint8Array
    let b = new Uint8Array(u.length);
    for (let k = 0; k < u.length; k++) {
        b[k] = u.charCodeAt(k)
    }
    return b
}

function binstring(b) {
    // convert a Uint8Array to a binary string
    let s = "";
    for (let k = 0; k < b.length; k++) {
        s += String.fromCharCode(b[k])
    }
    return s
}

function utf8Encode(str) {
    // convert a string to an Uint8Array of the UTF-8 representation
    let u = utf8In16Encode(str);
    return binary(u)
}

function b64EncodeUnicode(str) {
    // base64-encode the UTF-8 representation of a string
    return btoa(utf8In16Encode(str))
}

function b64DecodeUnicode(str) {
    // base64-decode a UTF-8 representation and return the string
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

function xor(b1, b2) {
    let b3 = new Uint8Array(b1.length);
    for (let k = 0; k < b1.length; k++) {
        b3[k] = b1[k] ^ b2[k]
    }
    return b3
}

function debugBinary(b) {
    let s = "[";
    for (let k = 0; k < b.length; k++) {
        if (k > 0) s += ",";
        s += b[k]
    };
    return s + "]"
}

function baseURL() {
    return document.location.protocol + "//" + document.location.hostname + ":" + document.location.port
}

function decodeWWWAuthenticate(s) {
    let re1 = /^\s*SCRAM-SHA-256\s+/g;
    let m1 = re1.exec(s);
    if (m1 == null) return null;
    s = s.slice(re1.lastIndex);
    let params = {};
    while (s.length > 0) {
        let re2 = /^\s*([a-z]+)\s*=\s*/g;
        let m2 = re2.exec(s);
        if (m2 == null) break;
        let name = m2[1];
        s = s.slice(re2.lastIndex);
        let value = "";
        if (s[0] == '"') {
            // FIXME: doesn't support escaping yet!
            s = s.slice(1);
            let re4 = /"/g;
            let m4 = re4.exec(s);
            if (m4 == null) break;
            value = s.slice(0, m4.index);
            s = s.slice(re4.lastIndex);
            s = s.trimLeft();
            if (s[0] == ",") s = s.slice(1);
        } else {
            let re3 = /\s*,/g;
            let m3 = re3.exec(s);
            if (m3 == null) {
                value = s.trimRight();
                s = ""
            } else {
                value = s.slice(0, m3.index);
                s = s.slice(re3.lastIndex);
            }
        }
        params[name] = value;
    }
    return params
}

async function pbkdf2(key, salt, i) {
    let u = new Array(i);
    // set u[0]:
    let n = salt.length;
    let b = new Uint8Array(n + 4);
    b.set(salt);
    b[n] = 0;
    b[n+1] = 0;
    b[n+2] = 0;
    b[n+3] = 1;
    u[0] = new Uint8Array(await crypto.subtle.sign("HMAC", key, b));
    console.log("u0 = " + debugBinary(u[0]));
    // compute rest of u
    for (let k = 1; k < i; k++) {
        u[k] = new Uint8Array(await crypto.subtle.sign("HMAC", key, u[k-1]))
    }
    // xor:
    let x = u[0];
    for (let k = 1; k < i; k++) x = xor(x, u[k]);
    return x
}

async function auth_crypto_step1(user, password, req, r, c1_bare) {
    if (req.status != 401) {
        console.log("Authentication: status " + req.status);
        throw "code 1"
    };
    let h = req.getResponseHeader("WWW-Authenticate");
    if (h == null) {
        console.log("Authentication response lacks WWW-Authenticate hdr");
        throw "code 2"
    };
    // expect: SCRAM-SHA-256 sid=..., data=...
    let params = decodeWWWAuthenticate(h);
    if (params == null) {
        console.log("cannot decode WWW-Authenticate: " + h)
        throw "code 3"
    };
    if (params["sid"] === undefined) {
        console.log("missing parameter: sid");
        throw "code 4"
    };
    if (params["data"] === undefined) {
        console.log("missing parameter: data");
        throw "code 5"
    };
    s1 = b64DecodeUnicode(params["data"])
    let re = /^r=([^,]+),s=([^,]+),i=([0-9]+)$/;
    let m = re.exec(s1);
    if (m == null) {
        console.log("cannot decode s1: " + s1);
        throw "code 6"
    };
    let s1_r = m[1];
    let s1_s = m[2];
    let s1_i = parseInt(m[3], 10);
    if (!s1_r.startsWith(r)) {
        console.log("invalid r from server: " + r);
        throw "code 7"
    };
    console.log("r=" + s1_r);
    console.log("s=" + s1_s);
    console.log("i=" + s1_i);
    console.log("length salt=" + binary(atob(s1_s)).length);
    let algo =
        { "name": "HMAC",
          "hash": { "name": "SHA-256" }
        };
    let c1_final_noproof = "c=biws,r=" + s1_r;
    let password_bytes = utf8Encode(password);
    console.log("password_bytes = " + debugBinary(password_bytes));
    let key = await crypto.subtle.importKey("raw", password_bytes, algo, false, [ "sign" ]).catch(asyncError("key"));
    let salted_password = await pbkdf2(key, binary(atob(s1_s)), s1_i).catch(asyncError("salted_password"));
    console.log("salted_password=" +  btoa(binstring(salted_password)));
    let salted_password_key = await crypto.subtle.importKey("raw", salted_password, algo, false, [ "sign" ]).catch(asyncError("salted_password_key"));
    let client_key = new Uint8Array(await crypto.subtle.sign("HMAC", salted_password_key, utf8Encode("Client Key")).catch(asyncError("client_key")));
    console.log("client_key=" +  btoa(binstring(client_key)));
    let stored_key = new Uint8Array(await crypto.subtle.digest("SHA-256", client_key).catch( asyncError("stored_key")));
    console.log("stored_key=" + debugBinary(stored_key));
    console.log("stored_key=" + btoa(binstring(stored_key)));
    let auth_message0 = c1_bare + "," + s1 + "," + c1_final_noproof;
    let auth_message = utf8Encode(auth_message0);
    console.log("auth_message=" + auth_message0);
    let stored_key_key = await crypto.subtle.importKey("raw", stored_key, algo, false, [ "sign" ]).catch(asyncError("stored_key_key"));
    let client_signature = new Uint8Array(await crypto.subtle.sign("HMAC", stored_key_key, auth_message).catch(asyncError("client_signature")));
    console.log("client_signature=" + debugBinary(client_signature));
    let client_proof = xor(client_key, client_signature);
    let p = btoa(binstring(client_proof));
    let c1_final = c1_final_noproof + ",p=" + p;
    console.log("c1_final=" + c1_final);
    let server_key = new Uint8Array(await crypto.subtle.sign("HMAC", salted_password_key, utf8Encode("Server Key")).catch(asyncError("server_key")));
    let server_key_key = await crypto.subtle.importKey("raw", server_key, algo, false, [ "sign" ]).catch(asyncError("server_key_key"));
    let server_signature = new Uint8Array(await crypto.subtle.sign("HMAC", server_key_key, auth_message).catch(asyncError("server_signature")));
    console.log("server_signature=" + debugBinary(server_signature));
    return { "sid": params["sid"],
             "c1_final": c1_final,
             "server_signature": btoa(binstring(server_signature))
           }
 }

async function auth_crypto_step2(props, req) {
    if (req.status != 200) {
        console.log("Authentication: status " + req.status);
        throw "code 11"
    };
    let h = req.getResponseHeader("Authentication-Info");
    if (h == null) {
        console.log("Authentication response lacks Authentication-Info hdr");
        throw "code 12"
    };
    // expect: sid=..., data=...
    let params = decodeWWWAuthenticate("SCRAM-SHA-256 " + h);
    if (params == null) {
        console.log("cannot decode Authentication-Info: " + h)
        throw "code 13"
    };
    if (params["sid"] === undefined) {
        console.log("missing parameter: sid");
        throw "code 14"
    };
    if (params["data"] === undefined) {
        console.log("missing parameter: data");
        throw "code 15"
    };
    if (params["sid"] != props.sid) {
        console.log("bad parameter: sid");
        throw "code 16"
    }
    s2 = b64DecodeUnicode(params["data"])
    let re = /^v=([^,]+)$/;
    let m = re.exec(s2);
    if (m == null) {
        console.log("cannot decode s2: " + s2);
        throw "code 17"
    };
    let v = m[1];
    if (v != props.server_signature) {
        console.log("bad server signature: " + v);
        throw "code 18"
    }
}

function authenticate(user, password, onsuccess, onerror) {
    let random = new Uint8Array(8);
    window.crypto.getRandomValues(random);
    let r = window.btoa(binstring(random));
    // r = "rOprNGfwEbeRWgbNEkqO"; // DEBUG
    let userEscaped = user.replace("=", "=3D").replace(",","=2C")
    let c1_bare = "n=" + userEscaped + ",r=" + r;
    let c1 = "n,," + c1_bare;
    let c1_b64 = b64EncodeUnicode(c1);
    let base = baseURL();
    let req1 = new XMLHttpRequest();
    req1.open("GET", base + "/auth/auth");
    req1.setRequestHeader("Authorization", "SCRAM-SHA-256 data=" + c1_b64)
    req1.onload = function() {
        auth_crypto_step1(user, password, req1, r, c1_bare).then(function(props) {
            console.log("next request");
            let req2 = new XMLHttpRequest();
            req2.open("GET", base + "/auth/auth");
            req2.setRequestHeader("Authorization", "SCRAM-SHA-256 sid=" + props.sid + ",data=" + b64EncodeUnicode(props.c1_final))
            req2.onload = function() {
                auth_crypto_step2(props, req2).then(function() {
                    console.log("successful!")
                }, onerror)
            };
            req2.send()
        }, onerror)
    };
    req1.send();
}

function asyncError(from) {
    return function(error) {
        console.log("Error " + from + "; " + error);
        throw error
    }
}

function authInteraction(form) {
    authenticate(form.user.value, form.password.value,
                 function () {
                     console.log("successfully logged in");
                 },
                 function (reason) {
                     console.log("authentication failed: " + reason);
                 })
}
