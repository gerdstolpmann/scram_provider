open Atypes
open Printf

let logged_in (cgi : Netcgi.cgi_activation) user =
  cgi # set_header
      ~status:`Ok
      ~cache:`No_cache
      ~content_type:"text/html; charset=\"utf-8\""
      ();
  Util.begin_page cgi "Logged in";
  cgi # output # output_string (Util.encode ("User name: " ^ user));
  Util.end_page cgi;
  cgi # output # commit_work()

let login_mask (cgi : Netcgi.cgi_activation) =
  cgi # set_header
      ~status:`Ok
      ~cache:`No_cache
      ~content_type:"text/html; charset=\"utf-8\""
      ();
  Util.begin_page cgi "Login";
  cgi # output # output_string "<script src='/auth/static/script.js'></script>\n";
  cgi # output # output_string "<form name='authform'>\n";
  cgi # output # output_string "User: <input type=text name=user><br>\n";
  cgi # output # output_string "Password: <input type=text name=password><br>\n";
  cgi # output # output_string "<input type=button name=submit value='Login' onclick=\"authInteraction(authform)\">\n";
  cgi # output # output_string "</form>\n";
  Util.end_page cgi;
  cgi # output # commit_work()


let process (cgi : Netcgi.cgi_activation) =
  match Sessions.check_auth cgi with
    | None ->
        login_mask cgi
    | Some user ->
        logged_in cgi user.user_id
